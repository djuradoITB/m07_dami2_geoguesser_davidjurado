package cat.itb.geoguesser;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView questionsTextView;
    private TextView answeredTextView;
    private ProgressBar progressBar;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button hint;

    private String[] questions;
    private String[] answers;
    private ArrayList<QuestionAnswer> quiz;
    private ArrayList<String> preguntesMostrades;
    private ArrayList<String> respostesMostrades;
    private Set<Integer> randNums;
    private int preguntaActual = 0;
    private int restantsPista = 3;
    private double score = 0;

    private final int NUM_PREGUNTES = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        questionsTextView = findViewById(R.id.questionTextView);
        answeredTextView = findViewById(R.id.questionsAnsweredTextView);
        progressBar = findViewById(R.id.progressBar);
        button1 = findViewById(R.id.option1Button);
        button2 = findViewById(R.id.option2Button);
        button3 = findViewById(R.id.option3Button);
        button4 = findViewById(R.id.option4Button);
        hint = findViewById(R.id.hintButton);

        Resources res = getResources();
        questions = res.getStringArray(R.array.questionsText);
        answers = res.getStringArray(R.array.answersText);
        quiz = new ArrayList<>();
        randNums = genPosNoRepeat();

        for (int i = 0; i < NUM_PREGUNTES; i++) {
            quiz.add(new QuestionAnswer(questions[i], answers[i]));
        }
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        hint.setOnClickListener(this);
        progressBar.setMax(100);
        inicialitzarPantalla();

    }

    private void inicialitzarPantalla() {
        preguntaActual = 1;
        int rand = getRandNumberPos();
        questionsTextView.setText(quiz.get(rand).getQuestion());
        answeredTextView.setText("Pregunta " + preguntaActual + " de " + NUM_PREGUNTES);
        assignarTextButons(rand, randButton());
        progressBar.setProgress(10);
        hint.setText("Pista: (" + restantsPista + ")");
    }


    private void avancarPregunta() {
        preguntaActual++;
        if (preguntaActual > 10) {
            finalitzar();
        } else {
            int rand = getRandNumberPos();
            questionsTextView.setText(quiz.get(rand).getQuestion());
            answeredTextView.setText("Pregunta " + preguntaActual + " de " + NUM_PREGUNTES);
            progressBar.incrementProgressBy(10);
            assignarTextButons(rand, randButton());
        }
    }

    private void finalitzar() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialog_title);
        builder.setMessage("Has obtingut una puntuació de " + Double.toString(score * 10) + "/ 100"); //No he pogut referenciar aqui ja que em saltava un bug
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private int getRandNumberPos() {
        int n = 0;
        if (preguntaActual != 0) {
            Iterator it = randNums.iterator();
            for (int i = 0; i < preguntaActual; i++) {
                n = (int) it.next();
            }
        }
        return n;
    }

    private void assignarTextButons(int array_pos, int rand_butt_selected) {
        int randButt1 = (int) (Math.random() * answers.length);
        int randButt2 = (int) (Math.random() * answers.length);
        int randButt3 = (int) (Math.random() * answers.length);
        switch (rand_butt_selected) {
            case 1:
                button1.setText(quiz.get(array_pos).getAnswer());
                button2.setText(answers[randButt1]);
                button3.setText(answers[randButt2]);
                button4.setText(answers[randButt3]);
                break;
            case 2:
                button1.setText(answers[randButt1]);
                button2.setText(quiz.get(array_pos).getAnswer());
                button3.setText(answers[randButt2]);
                button4.setText(answers[randButt3]);
                break;
            case 3:
                button1.setText(answers[randButt1]);
                button2.setText(answers[randButt2]);
                button3.setText(quiz.get(array_pos).getAnswer());
                button4.setText(answers[randButt3]);
                break;
            case 4:
                button1.setText(answers[randButt1]);
                button2.setText(answers[randButt2]);
                button3.setText(answers[randButt3]);
                button4.setText(quiz.get(array_pos).getAnswer());
                break;
        }
    }

    private int randButton() {
        int num = (int) ((Math.random() * 4) + 1);
        return num;
    }

    @Override
    public void onClick(View v) {
        boolean encert = false;
        switch (v.getId()) {
            case R.id.option1Button:
                encert = isCorrect(button1.getText().toString(), quiz.get(getRandNumberPos()).getAnswer());
                avancarPregunta();
                mostrarResposta(encert);
                break;
            case R.id.option2Button:
                encert = isCorrect(button2.getText().toString(), quiz.get(getRandNumberPos()).getAnswer());
                avancarPregunta();
                mostrarResposta(encert);
                break;
            case R.id.option3Button:
                encert = isCorrect(button3.getText().toString(), quiz.get(getRandNumberPos()).getAnswer());
                avancarPregunta();
                mostrarResposta(encert);
                break;
            case R.id.option4Button:
                encert = isCorrect(button4.getText().toString(), quiz.get(getRandNumberPos()).getAnswer());
                avancarPregunta();
                mostrarResposta(encert);
                break;
            case R.id.hintButton:
                if (restantsPista > 0) {
                    int rand = getRandNumberPos();
                    Toast.makeText(MainActivity.this, "La resposta correcta era: " + quiz.get(rand).getAnswer(), Toast.LENGTH_SHORT).show();
                    avancarPregunta();
                    restantsPista--;
                    hint.setText("Pista: (" + restantsPista + ")");
                    if (restantsPista == 0) {
                        hint.setVisibility(View.INVISIBLE);
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No et queden pistes restants", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean isCorrect(String answer, String correctAnswer) {
        boolean isCorrect;
        if (answer.equals(correctAnswer)) {
            isCorrect = true;
            score += 1;
        } else {
            isCorrect = false;
            score -= 0.5;
        }
        return isCorrect;
    }

    private void mostrarResposta(boolean encert) {
        if (encert)
            Toast.makeText(MainActivity.this, R.string.respostaCorrecte, Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(MainActivity.this, R.string.respostaIncorrecte, Toast.LENGTH_SHORT).show();
    }

    private Set<Integer> genPosNoRepeat() {
        Random rng = new Random();
        Set<Integer> generated = new LinkedHashSet<Integer>();
        while (generated.size() < NUM_PREGUNTES) {
            Integer num = rng.nextInt(NUM_PREGUNTES);
            generated.add(num);
        }
        return generated;
    }


}